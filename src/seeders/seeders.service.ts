import { PlaceService } from './../place/place.service';
import { UsersService } from './../users/users.service';

import { Injectable } from '@nestjs/common';
import { ReportService } from '../report/report.service';

@Injectable()
export class SeedersService {
  constructor(
    private readonly userServise: UsersService,
    private readonly placeServise: PlaceService,
    private readonly reportService: ReportService
  ) {}

  async userCreate(users) {
    for (let i = 0; i < users.length; i++) {
      await this.userServise.create(users[i]);
    }
  }

  async placementCreate(places) {
    for (let i = 0; i < places.length; i++) {
      await this.placeServise.createPlace(places[i], {
        user: { uuid: places[i].user_uuid },
      });
    }
  }

  async greenhouseCreate(data) {
    for (let i = 0; i < data.length; i++) {
      await this.placeServise.createGreenhouse(data[i], {
        user: { uuid: data[i].user_uuid },
      });
    }
  }

  async reaportCreator(data) {
    for (let i = 0; i < data.length; i++) {
      await this.reportService.createForInitData(data[i]);
    }
  }
}
