import { UsersModule } from './../users/users.module';
import { Module } from '@nestjs/common';
import { SeedersService } from './seeders.service';
import { PlaceModule } from '../place/place.module';
import { ReportModule } from '../report/report.module';

@Module({
  imports: [UsersModule, PlaceModule, ReportModule],
  providers: [SeedersService],
})
export class SeedersModule {}
