import { Module } from '@nestjs/common';
import { DirectorDataService } from './director-data.service';
import { DirectorDataController } from './director-data.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [DirectorDataController],
  providers: [DirectorDataService, PrismaService],
 })
export class DirectorDataModule {}
