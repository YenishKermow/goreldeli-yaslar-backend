import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Header,
  Query,
  Res,
  Put,
} from '@nestjs/common';
import { roles } from '@prisma/client';
import { Response } from 'express';
import { Roles } from 'src/common/decorators/roles.decorators';
import { PaginationQueryDto } from 'src/common/pagination/query.dto';
import { DirectorDataService } from './director-data.service';
import { CreateDirectorDataDto } from './dto/create-director-data.dto';
import { UpdateDirectorDataDto } from './dto/update-director-data.dto';

@Roles(roles.admin)
@Controller('director-data')
export class DirectorDataController {
  constructor(private readonly directorDataService: DirectorDataService) {}
  
  @Post()
  create(@Body() createDirectorDateDto: CreateDirectorDataDto) {
    return this.directorDataService.create(createDirectorDateDto);
  }

  @Header('Access-Control-Expose-Headers', '*')
  @Get()
  async findAll(@Query() query: PaginationQueryDto, @Res() response: Response) {
    const departments = await this.directorDataService.findAll(query);
    response.header('Content-range', departments.count.toString());
    response.send(departments.result);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.directorDataService.findOne(+id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateDirectorDateDto: UpdateDirectorDataDto) {
    return this.directorDataService.update(+id, updateDirectorDateDto);
  }


  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.directorDataService.remove(+id);
  }
}
