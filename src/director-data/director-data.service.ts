import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { PaginationQueryDto } from 'src/common/pagination/query.dto';
import { PrismaService } from 'src/prisma.service';
import { CreateDirectorDataDto } from './dto/create-director-data.dto';
import { UpdateDirectorDataDto } from './dto/update-director-data.dto';

@Injectable()
export class DirectorDataService {
  constructor(private prisma: PrismaService) {}

  async create(createDirectorDataDto: CreateDirectorDataDto) {
    const data = await this.prisma.directorData.findMany();
    if (data.length) throw new BadRequestException('Директор уже существует');

    return await this.prisma.directorData.create({
      data: {
        ...createDirectorDataDto,
      },
    });
  }

  async findAll(query: PaginationQueryDto) {
    const filter = JSON.parse(query.filter);

    const count = await this.prisma.directorData.count();

    const data = await this.prisma.directorData.findMany();

    return {
      result: data,
      count,
    };
  }

  async findOne(id: number) {
    const data = await this.prisma.directorData.findUnique({
      where: { id },
    });

    if (!data) {
      throw new NotFoundException('Нет такого директора');
    }

    return data;
  }

  async update(id: number, updateDirectorDataDto: UpdateDirectorDataDto) {
    const data = await this.prisma.directorData.findUnique({
      where: { id },
    });

    if (!data) {
      throw new NotFoundException('Нет такого директора');
    }

    return this.prisma.directorData.update({
      where: { id },
      data: {
        firstnameTm: updateDirectorDataDto.firstnameTm,
        lastnameTm: updateDirectorDataDto.lastnameTm,
        firstnameRu: updateDirectorDataDto.firstnameRu,
        lastnameRu: updateDirectorDataDto.lastnameRu,
      },
    });
  }

  remove(id: number) {
    return this.prisma.directorData.delete({ where: { id } });
  }
}
