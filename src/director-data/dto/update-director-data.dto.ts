import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';
import { CreateDirectorDataDto } from './create-director-data.dto';

export class UpdateDirectorDataDto extends PartialType(CreateDirectorDataDto) {
    @ApiProperty()
    @IsNumber()
    id: number;
}
