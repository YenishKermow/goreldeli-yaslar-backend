import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateDirectorDataDto {
  @ApiProperty()
  @IsString()
  firstnameTm: string;

  @ApiProperty()
  @IsString()
  lastnameTm: string;

  @ApiProperty()
  @IsString()
  firstnameRu: string;

  @ApiProperty()
  @IsString()
  lastnameRu: string;
}
