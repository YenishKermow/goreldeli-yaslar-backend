import { ApiProperty } from '@nestjs/swagger';
import { IsString, MaxLength, MinLength } from 'class-validator';

export class CreateAuthDto {
  @ApiProperty({ description: 'Логин', default: '+99365666666' })
  @IsString()
  @MaxLength(12)
  @MinLength(12)
  phone: string;

  @ApiProperty({ description: 'Пароль', default: 'admin' })
  @IsString()
  password: string;
}
