import { PrismaService } from './../prisma.service';
import { Bcrypt } from './../common/utils/bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { CreateAuthDto } from './dto/create-auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly bcrypt: Bcrypt,
    private readonly jwtService: JwtService,
    private prisma: PrismaService,
  ) {}

  async login({ phone, password }: CreateAuthDto): Promise<any> {
    const user = await this.prisma.users.findUnique({ where: { phone } });
    if (user) {
      const decrypt = await this.bcrypt.decryptCode(password, user.password);
      if (decrypt) {
        delete user.password;
        const { uuid, ...result } = user;
        const payload = { uuid, role: user.role };
        const token = this.jwtService.sign(payload);
        return { result, token };
      } else {
        throw new UnauthorizedException('Не правильный пароль');
      }
    } else {
      throw new UnauthorizedException(
        'Пользователь с таким номером не найдено',
      );
    }
  }
}
