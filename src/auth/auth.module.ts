import { PrismaService } from './../prisma.service';
import { Bcrypt } from './../common/utils/bcrypt';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';

@Module({
  imports: [
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: process.env.SECRET_TOKEN,
      }),
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, Bcrypt, PrismaService],
})
export class AuthModule {}
