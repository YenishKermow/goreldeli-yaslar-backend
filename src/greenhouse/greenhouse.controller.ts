import { Controller, Get, Header, Query, Res } from '@nestjs/common';
import { Response } from 'express';
import { PaginationQueryDto } from 'src/common/pagination/query.dto';
import { GreenhouseService } from './greenhouse.service';

@Controller('greenhouse')
export class GreenhouseController {
  constructor(private readonly greenhouseService: GreenhouseService) {}
  
  @Header('Access-Control-Expose-Headers', '*')
  @Get()
  async findAllGreenHouse(@Query() query: PaginationQueryDto, @Res() response: Response) {
    const greenHouses = await this.greenhouseService.findAllGreenHouse(query);
    response.header('Content-range', greenHouses.count.toString());
    response.send(greenHouses.result);
  }
}
