import { Injectable } from '@nestjs/common';
import { PaginationQueryDto } from 'src/common/pagination/query.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class GreenhouseService {
  constructor(private prisma: PrismaService) {}

  async findAllGreenHouse(query: PaginationQueryDto) {
    const filter = JSON.parse(query.filter);
    const { id, q } = filter;

    const count = await await this.prisma.greenhouse.count({
      where: {
        id: id && id.length ? id[0] : undefined,
        location: {
          contains: q,
        },
      },
    });

    const departments = await this.prisma.greenhouse.findMany({
      where: {
        id: id && id.length ? id[0] : undefined,
        location: {
          contains: q,
        },
      },
    });

    return {
      result: departments,
      count,
    };
  }
}
