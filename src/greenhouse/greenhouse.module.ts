import { Module } from '@nestjs/common';
import { GreenhouseService } from './greenhouse.service';
import { GreenhouseController } from './greenhouse.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [GreenhouseController],
  providers: [GreenhouseService, PrismaService]
})
export class GreenhouseModule {}
