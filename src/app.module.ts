import { PrismaService } from './prisma.service';
import { TasksService } from './task.service';
import { SeedersModule } from './seeders/seeders.module';
import { CommonModule } from './common/common.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { PlaceModule } from './place/place.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CheckModule } from './check/check.module';
import { ReportModule } from './report/report.module';
import { DepartmentsModule } from './departments/departments.module';
import { RentersModule } from './renters/renters.module';
import { GreenhouseModule } from './greenhouse/greenhouse.module';
import { DirectorDataModule } from './director-data/director-data.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ".env.production",
    }),
    ScheduleModule.forRoot(),
    CommonModule,
    AuthModule,
    UsersModule,
    PlaceModule,
    SeedersModule,
    CheckModule,
    ReportModule,
    DepartmentsModule,
    RentersModule,
    GreenhouseModule,
    DirectorDataModule,
  ],
  controllers: [],
  providers: [TasksService, PrismaService],
})
export class AppModule {}
