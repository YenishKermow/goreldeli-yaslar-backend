import { NestFactory } from '@nestjs/core';
import { SeedersModule } from './seeders/seeders.module';
import { SeedersService } from './seeders/seeders.service';
import * as users from '../data/users.json';
import * as places from '../data/placement.json';
import * as greenhouses from '../data/greenhouse.json';
import * as reports from '../data/reports.json';

async function bootstrap() {
  const seedersModule = await NestFactory.createApplicationContext(
    SeedersModule,
  );
  console.time();

  const seeder: SeedersService = seedersModule.get(SeedersService);
  await Promise.all([seeder.userCreate(users)]).catch(console.error);
  await Promise.all([
    seeder.placementCreate(places),
    seeder.greenhouseCreate(greenhouses),
  ]).catch(console.error);
  await Promise.all([seeder.reaportCreator(reports)]).catch(console.error);


  process.on('unhandledRejection', (err) => {
    console.log(err);
    console.log(
      'Данные не записались очистите базу и повторите процедуры. Повторяется уникальный код!',
    );
    process.exit();
  });

  console.timeEnd();

  process.exit(0);
}

bootstrap();
