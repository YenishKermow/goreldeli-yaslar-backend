import { UpdateCheckDto } from './dto/update-check.dto';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { PaginationQueryDto } from './../common/pagination/query.dto';
import {
  Controller,
  Get,
  Body,
  Param,
  Query,
  Res,
  Header,
  Put,
  Req,
  Post,
  Delete,
} from '@nestjs/common';
import { CheckService } from './check.service';
import { Request, Response } from 'express';
import { check, roles } from '@prisma/client';
import { Roles } from '../common/decorators/roles.decorators';
import { CreateCheckDto } from './dto/create-check.dto';
import { CreateCheckDtoForCashsier } from './dto/check-for-cashsier.dto';

@ApiTags('Помещении')
@ApiBearerAuth()
@Controller('check')
export class CheckController {
  declare check: Partial<check>;
  constructor(private readonly checkService: CheckService) {
    this.check = this.check;
  }

  @Roles(roles.cashier)
  @Post()
  @Header('Access-Control-Expose-Headers', '*')
  create(@Body() creataCheckDto : CreateCheckDtoForCashsier,  @Req() request: Request<any>) {
    return this.checkService.create(creataCheckDto, request.user);
  }

  @Get()
  @Header('Access-Control-Expose-Headers', '*')
  async findAll(@Query() query: PaginationQueryDto, @Res() response: Response, @Req() request: Request<any>) {
    const checks = await this.checkService.findAll(query, request.user);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('all')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllPaidChecks(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const checks = await this.checkService.findChecks(query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('unpaid')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllUnpaidChecksInMonth(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const checks = await this.checkService.findUnpaidChecks(query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('paid')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllPaidChecksInMonth(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const checks = await this.checkService.findPaidChecks(query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('dashboard')
  async dashbordChecks() {
    return await this.checkService.dashboard();
  }

  @Get(':id')
  @Header('Access-Control-Expose-Headers', '*')
  findOne(@Param('id') id: string, @Req() request: Request<any>) {
    return this.checkService.findOne(+id, request.user);
  }

  @Roles(roles.cashier, roles.director)
  @Put(':id')
  @Header('Access-Control-Expose-Headers', '*')
  async updateCheck(
    @Param('id') id: string,
    @Body() updateCheckDto: Partial<check>,
    @Req() request: Request<any>
  ) {
    return await this.checkService.updateCheckById(+id, updateCheckDto, request.user);
  }

  @Roles(roles.manager)
  @Delete(':id')
  @Header('Access-Control-Expose-Headers', '*')
  async deleteCheck(
    @Param('id') id: string,
    @Req() request: Request<any>
  ) {
    return await this.checkService.deleteOne(+id, request.user);
  }

  @Get('office/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllOfficeChecks(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
    @Param('id') id: string
  ) {
    const checks = await this.checkService.findPlacementChecks(+id, query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('custom_house/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllCustomHouseChecks(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
    @Param('id') id: string
  ) {
    const checks = await this.checkService.findPlacementChecks(+id, query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('product-place/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllProductPlaceChecks(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
    @Param('id') id: string
  ) {
    const checks = await this.checkService.findPlacementChecks(+id, query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('warehouse/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllWarehouseChecks(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
    @Param('id') id: string
  ) {
    const checks = await this.checkService.findPlacementChecks(+id, query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('dinning-room/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllDinningRoomChecks(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
    @Param('id') id: string
  ) {
    const checks = await this.checkService.findPlacementChecks(+id, query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Get('all/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findOnePaidChecks(@Param('id') id: string) {
    return await this.checkService.findCheck(+id);
  }

  @Get('unpaid/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findOneUnpaidChecksInMonth(@Param('id') id: string) {
    return await this.checkService.findUnpaidCheck(+id);
  }

  @Get('paid/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async findOnePaidChecksInMonth(@Param('id') id: string) {
    return await this.checkService.findPaidCheck(+id);
  }

  @Put('all/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async updatePaidCheck(
    @Param('id') id: string,
    @Body() updateCheckDto: Partial<check>,
  ) {
    return await this.checkService.updateCheck(+id, updateCheckDto);
  }

  @Put('unpaid/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async updateUnpaidCheckInMonth(
    @Param('id') id: string,
    @Body() updateCheckDto: UpdateCheckDto,
  ) {
    return await this.checkService.updateUnpaidCheck(+id, updateCheckDto);
  }

  @Put('paid/:id')
  @Header('Access-Control-Expose-Headers', '*')
  async updatePaidCheckInMonth(
    @Param('id') id: string,
    @Body() updateCheckDto: Partial<check>,
  ) {
    return await this.checkService.updatePaidCheck(+id, updateCheckDto);
  }
}
