import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional, IsString } from 'class-validator';

export class CreateCheckDto {
  @ApiProperty()
  @IsString()
  location: string;

  @ApiProperty()
  @IsString()
  totalArea: string;

  @ApiProperty()
  @IsString()
  shop_name: string;

  @ApiProperty()
  @IsString()
  firstname: string;

  @ApiProperty()
  @IsString()
  lastname: string;

  @ApiProperty()
  @IsOptional()
  in?: string;

  @ApiProperty()
  @IsOptional()
  to?: string;

  @ApiProperty()
  // @IsDateString()
  date: Date;

  @ApiProperty()
  @IsString()
  payment_date: string;

  @ApiProperty()
  @IsString()
  peni: string;

  @ApiProperty()
  @IsString()
  rent: string;

  @ApiProperty()
  @IsOptional()
  sum_tm?: string;

  @ApiProperty()
  @IsOptional()
  sum_ru?: string;
}
