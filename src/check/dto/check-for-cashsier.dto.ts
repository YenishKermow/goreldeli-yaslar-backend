import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateCheckDtoForCashsier {
  @ApiProperty()
  @IsNumber()
  department_id: number;
  
  @ApiProperty()
  @IsNumber()
  renter_id: number;

  @ApiProperty()
  @IsString()
  shop_name: string;

  @ApiProperty()
  @IsOptional()
  in?: string;

  @ApiProperty()
  @IsOptional()
  to?: string;

  @ApiProperty()
  @IsString()
  payment_date: string;

  @ApiProperty()
  @IsString()
  peni: string;

  @ApiProperty()
  @IsString()
  rent: string;

  @ApiProperty()
  @IsOptional()
  sum_tm?: string;

  @ApiProperty()
  @IsOptional()
  sum_ru?: string;
}
