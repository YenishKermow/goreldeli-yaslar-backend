import { PartialType, ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';
import { CreateCheckDto } from './create-check.dto';

export class UpdateCheckDto extends PartialType(CreateCheckDto) {
  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  status?: boolean;

  @ApiProperty()
  @IsOptional()
  sum_tm?: string;

  @ApiProperty()
  @IsOptional()
  sum_ru?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  water?: string;

  @ApiProperty()
  @IsString()
  amperage?: string;

  @ApiProperty()
  @IsString()
  communal?: string;
}
