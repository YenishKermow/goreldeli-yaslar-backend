import { PaginationQueryDto } from './../common/pagination/query.dto';
import { PrismaService } from './../prisma.service';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { check } from '.prisma/client';
import { FormatDate } from 'src/utils/format-date';
import { CreateCheckDtoForCashsier } from './dto/check-for-cashsier.dto';
import { UpdateCheckDto } from './dto/update-check.dto';

@Injectable()
export class CheckService {
  constructor(
    private prisma: PrismaService,
    private readonly formatDate: FormatDate,
  ) {}

  async create(creataCheckDto: CreateCheckDtoForCashsier, user) {
    const department = await this.prisma.placement.findUnique({
      where: {
        id: creataCheckDto.department_id,
      },
    });

    const renter = await this.prisma.renter.findUnique({
      where: {
        id: creataCheckDto.renter_id,
      },
      include: {
        check: true,
      },
    });

    const paidCheck = await this.prisma.check.findMany({
      where: {
        renter: {
          id: renter.id,
        },
        in: {
          gte: creataCheckDto.in,
        },
        to: {
          lte: creataCheckDto.to,
        },
      },
    });

    if (paidCheck.length) {
      throw new BadRequestException('На эти даты уже оплачено!!!');
    }

    await this.prisma.check.updateMany({
      where: {
        renter: {
          id: renter.id,
        },
        in: {
          gte: creataCheckDto.in,
        },
        to: {
          lte: creataCheckDto.to,
        },
      },
      data: {
        status: true,
        active: false,
      },
    });

    const check = await this.prisma.check.create({
      data: {
        placement: {
          connect: {
            id: department.id,
          },
        },
        renter: {
          connect: {
            id: renter.id,
          },
        },
        date: new Date(),
        payment_date: creataCheckDto.payment_date,
        in: creataCheckDto.in,
        to: creataCheckDto.to,
        location: department.location,
        totalArea: department.totalArea,
        firstname: renter.firstname,
        lastname: renter.lastname,
        shop_name: renter.shopName,
        communal: department.communal,
        number_contract: renter.number_contract,
        water: department.water,
        amperage: department.amperage,
        peni: creataCheckDto.peni,
        sum_ru: creataCheckDto.sum_ru,
        sum_tm: creataCheckDto.sum_tm,
        status: true,
        cashier: {
          connect: {
            uuid_user: user.uuid,
          },
        },
        rent: creataCheckDto.rent,
      },
    });

    return check;
  }

  async findAll(query: PaginationQueryDto, user) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    const { shop_name, location, status, id, date, cashier, verify, renter } =
      filter;
    const lastname = cashier ? cashier.lastname : undefined;
    const firstname = cashier ? cashier.firstname : undefined;
    const payment = this.formatDate.checkDate(date);

    const count = await this.prisma.check.count({
      where: {
        active: true,
        shop_name: {
          contains: shop_name,
        },
        location: {
          contains: location,
        },
        verify,
        status,
        placement: {
          id,
        },
        payment_date: {
          gte: payment ? payment.toISOString().split('T')[0] : undefined,
        },
        renter: renter
          ? (() => {
              return {
                firstname: {
                  contains: renter.firstname,
                },
                phone: {
                  contains: renter.phone,
                },
              };
            })()
          : undefined,
        cashier: cashier
          ? (() => {
              return {
                firstname: {
                  contains: firstname,
                },
                lastname: {
                  contains: lastname,
                },
              };
            })()
          : undefined,
      },
    });

    let checks = await this.prisma.check.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        shop_name: {
          contains: shop_name,
        },
        location: {
          contains: location,
        },
        status,
        verify,
        placement: {
          id,
        },
        payment_date: {
          gte: payment ? payment.toISOString().split('T')[0] : undefined,
        },
        renter: renter
          ? (() => {
              return {
                firstname: {
                  contains: renter.firstname,
                },
                phone: {
                  contains: renter.phone,
                },
              };
            })()
          : undefined,
        cashier: cashier
          ? (() => {
              return {
                firstname: {
                  contains: firstname,
                },
                lastname: {
                  contains: lastname,
                },
              };
            })()
          : undefined,
      },
      include: {
        cashier: true,
        renter: true,
      },
    });

    const director = await this.prisma.directorData.findFirst();

    const firstnameDirectorTm = director ? director.firstnameTm : 'Nuryýew';
    const lastnameDirectorTm = director ? director.lastnameTm : 'B.';

    const firstnameDirectorRu = director ? director.firstnameRu : 'Нурыев';
    const lastnameDirectorRu = director ? director.lastnameRu : 'Б.';

    checks = checks.map((check) => {
      check['fullnameDirectorTm'] =
        lastnameDirectorTm + ' ' + firstnameDirectorTm;
      check['fullnameDirectorRu'] =
        lastnameDirectorRu + ' ' + firstnameDirectorRu;
      return check;
    });

    return { result: checks, count };
  }

  async findChecks(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);

    const count = await this.prisma.check.count({
      where: {
        active: true,
        status: true,
      },
    });

    const checks = await this.prisma.check.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        status: true,
      },
    });

    return { result: checks, count };
  }

  async findPaidChecks(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);

    const count = await this.prisma.check.count({
      where: {
        active: true,
        status: true,
      },
    });

    const checks = await this.prisma.check.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        status: true,
      },
    });

    return { result: checks, count };
  }

  async findUnpaidChecks(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);

    const count = await this.prisma.check.count({
      where: {
        active: true,
        status: false,
      },
    });

    const checks = await this.prisma.check.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        status: false,
      },
    });

    return { result: checks, count };
  }

  async findPlacementChecks(id: number, query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);

    const count = await this.prisma.check.count({
      where: {
        active: true,
        status: true,
        placement: {
          id,
        },
      },
    });

    const checks = await this.prisma.check.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        placement: {
          id,
        },
      },
    });

    return { result: checks, count };
  }

  async dashboard() {
    const unpaid = await this.prisma.check.count({
      where: {
        active: true,
        status: false,
      },
    });

    const paid = await this.prisma.check.count({
      where: {
        active: true,
        status: true,
      },
    });

    const allChecks = await this.prisma.check.count({
      where: {
        active: true,
        status: true,
      },
    });

    return {
      paidInMonth: paid,
      unpaidInMonth: unpaid,
      allPaidChecks: allChecks,
    };
  }

  async findCheck(id: number) {
    const check = await this.prisma.check.findMany({
      where: {
        active: true,
        status: true,
        id,
      },
    });

    if (!check.length) {
      throw new NotFoundException('Нет такого Чека');
    }

    return check[0];
  }

  async findPaidCheck(id: number) {
    const check = await this.prisma.check.findMany({
      where: {
        active: true,
        status: true,
        id,
      },
    });

    if (!check.length) {
      throw new NotFoundException('Нет такого Чека');
    }

    return check[0];
  }

  async findUnpaidCheck(id: number) {
    const check = await this.prisma.check.findMany({
      where: {
        active: true,
        status: false,
        id,
      },
    });

    if (!check.length) {
      throw new NotFoundException('Нет такого Чека');
    }

    return check[0];
  }

  async updateCheck(id: number, updateCheckDto: Partial<check>) {
    const check = await this.prisma.check.updateMany({
      where: {
        active: true,
        status: true,
        id,
      },
      data: {
        location: updateCheckDto.location,
        totalArea: updateCheckDto.totalArea,
        shop_name: updateCheckDto.shop_name,
        firstname: updateCheckDto.firstname,
        lastname: updateCheckDto.lastname,
        date: updateCheckDto.date,
        payment_date: updateCheckDto.payment_date,
        peni: updateCheckDto.peni,
        rent: updateCheckDto.rent,
        water: updateCheckDto.water,
        amperage: updateCheckDto.amperage,
        communal: updateCheckDto.communal,
      },
    });

    return check;
  }

  async updatePaidCheck(id: number, updateCheckDto: Partial<check>) {
    const check = await this.prisma.check.updateMany({
      where: {
        active: true,
        status: true,
        id,
      },
      data: {
        location: updateCheckDto.location,
        totalArea: updateCheckDto.totalArea,
        shop_name: updateCheckDto.shop_name,
        firstname: updateCheckDto.firstname,
        lastname: updateCheckDto.lastname,
        date: updateCheckDto.date,
        payment_date: updateCheckDto.payment_date,
        peni: updateCheckDto.peni,
        rent: updateCheckDto.rent,
      },
    });

    return check;
  }

  async updateUnpaidCheck(id: number, updateCheckDto: UpdateCheckDto) {
    const check = await this.prisma.check.updateMany({
      where: {
        active: true,
        status: false,
        id,
      },
      data: {
        location: updateCheckDto.location,
        totalArea: updateCheckDto.totalArea,
        shop_name: updateCheckDto.shop_name,
        firstname: updateCheckDto.firstname,
        lastname: updateCheckDto.lastname,
        date: updateCheckDto.date,
        payment_date: updateCheckDto.payment_date,
        peni: updateCheckDto.peni,
        rent: updateCheckDto.rent,
        status: updateCheckDto.status,
      },
    });

    return check;
  }

  async updateCheckById(id: number, updateCheckDto: Partial<check>, user) {
    const check = await this.prisma.check.update({
      where: {
        id,
      },
      data: {
        location: updateCheckDto.location,
        totalArea: updateCheckDto.totalArea,
        shop_name: updateCheckDto.shop_name,
        firstname: updateCheckDto.firstname,
        lastname: updateCheckDto.lastname,
        date: updateCheckDto.date,
        payment_date: updateCheckDto.payment_date,
        peni: updateCheckDto.peni,
        rent: updateCheckDto.rent,
        status: updateCheckDto.status,
        sum_ru: updateCheckDto.sum_ru,
        sum_tm: updateCheckDto.sum_tm,
        water: updateCheckDto.water,
        amperage: updateCheckDto.amperage,
        communal: updateCheckDto.communal,
        in: updateCheckDto.in,
        to: updateCheckDto.to,
        cashier:
          user.role === 'cashier'
            ? {
                connect: {
                  uuid_user: user.uuid,
                },
              }
            : undefined,
        verify: updateCheckDto.verify,
      },
    });

    return check;
  }

  async findOne(id: number, user) {
    const check = await this.prisma.check.findMany({
      where: {
        active: true,
        id,
      },
      include: {
        cashier: true,
        renter: true,
      },
    });

    if (!check.length) {
      throw new NotFoundException('Нет такого Чека');
    }

    if (user.role === 'director') {
      await this.prisma.check.updateMany({
        where: {
          id,
          visible: true,
          active: true,
        },
        data: {
          visible: false,
        },
      });
    }

    return check[0];
  }

  async deleteOne(id:number, user) {

    if(user.role !== 'manager'){
      throw new UnauthorizedException('У вас нет доcтупа к этой действий')
    }

    return await this.prisma.check.delete({
      where: {
        id
      }
    })

  }
}
