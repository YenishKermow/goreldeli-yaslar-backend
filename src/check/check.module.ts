import { PrismaService } from './../prisma.service';
import { Module } from '@nestjs/common';
import { CheckService } from './check.service';
import { CheckController } from './check.controller';
import { FormatDate } from 'src/utils/format-date';

@Module({
  controllers: [CheckController],
  providers: [CheckService, PrismaService, FormatDate],
})
export class CheckModule {}
