import { Controller, Get, Query, Header, Res } from '@nestjs/common';
import { Response } from 'express';
import { PaginationQueryDto } from 'src/common/pagination/query.dto';
import { DepartmentsService } from './departments.service';

@Controller('departments')
export class DepartmentsController {
  constructor(private readonly departmentsService: DepartmentsService) {}

  @Header('Access-Control-Expose-Headers', '*')
  @Get()
  async findAll(@Query() query: PaginationQueryDto, @Res() response: Response) {
    const departments = await this.departmentsService.findAll(query);
    response.header('Content-range', departments.count.toString());
    response.send(departments.result);
  }
}
