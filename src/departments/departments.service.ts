import { Injectable } from '@nestjs/common';
import { PaginationQueryDto } from 'src/common/pagination/query.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class DepartmentsService {
  constructor(private prisma: PrismaService) {}

  async findAll(query: PaginationQueryDto) {
    const filter = JSON.parse(query.filter);
    const { id } = filter;

    const count = await this.prisma.placement.count({
      where: {
        id: id && id.length ? id[0] : undefined,
        renter: {
          some: {
            active: true
          }
        }
      },
    });

    const departments = await this.prisma.placement.findMany({
      where: {
        id: id && id.length ? id[0] : undefined,
        renter: {
          some: {
            active: true
          }
        }
      },
      include: {
        renter: true,
      },
    });

    return {
      result: departments,
      count,
    };
  }
}
