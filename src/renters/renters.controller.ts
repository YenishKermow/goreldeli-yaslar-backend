import { Controller, Get, Header, Query, Res } from '@nestjs/common';
import { Response } from 'express';
import { PaginationQueryDto } from '../common/pagination/query.dto';
import { RentersService } from './renters.service';

@Controller('renters')
export class RentersController {
  constructor(private readonly rentersService: RentersService) {}

  @Header('Access-Control-Expose-Headers', '*')
  @Get()
  async findAll(@Query() query: PaginationQueryDto, @Res() response: Response) {
    const departments = await this.rentersService.findAll(query);
    response.header('Content-range', departments.count.toString());
    response.send(departments.result);
    
  }


  @Header('Access-Control-Expose-Headers', '*')
  @Get('shop')
  async findAllRenters(@Query() query: PaginationQueryDto, @Res() response: Response) {
    const departments = await this.rentersService.findAllRenters(query);
    response.header('Content-range', departments.count.toString());
    response.send(departments.result);
    
  }
}
