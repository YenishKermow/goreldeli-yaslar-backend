import { Injectable } from '@nestjs/common';
import { PaginationQueryDto } from '../common/pagination/query.dto';
import { PrismaService } from '../prisma.service';

@Injectable()
export class RentersService {
  constructor(private prisma: PrismaService) {}

  async findAll(query: PaginationQueryDto) {
    const filter = JSON.parse(query.filter);
    const { id, department_id } = filter;

    const count = await await this.prisma.renter.count({
      where: {
        id: id && id.length ? id[0] : undefined,
        place: {
          id: department_id || undefined
        }
      },
    });

    const departments = await this.prisma.renter.findMany({
      where: {
        id: id && id.length ? id[0] : undefined,
        id_place:  department_id || undefined
      },
    });

    return {
      result: departments,
      count,
    };
  }

  async findAllRenters(query: PaginationQueryDto) {
    const filter = JSON.parse(query.filter);
    const { shop_name} = filter;

    const count = await await this.prisma.renter.count({
      where: {
        id: shop_name
      },
    });

    const departments = await this.prisma.renter.findMany({
      where: {
        id: shop_name
      },
    });

    return {
      result: departments,
      count,
    };
  }
}
