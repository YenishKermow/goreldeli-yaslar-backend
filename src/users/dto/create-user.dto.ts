import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
  ValidateNested,
} from 'class-validator';

export class CreateUserDto {
  @ApiProperty({ default: '+99362803369' })
  @IsString()
  @MaxLength(12)
  @MinLength(12)
  phone: string;

  @ApiProperty({ default: 'Arslan' })
  @IsString()
  firstname: string;

  @ApiProperty({ default: 'Abayew' })
  @IsString()
  lastname: string;

  @IsOptional()
  @IsString()
  password: string;
}

export class CreateUpdateUserDto {
  @ApiProperty({ default: 1 })
  @IsNumber()
  id: number;

  @ApiProperty({ type: () => CreateUserDto })
  @ValidateNested({ each: true })
  @Type(() => CreateUserDto)
  data: CreateUserDto;
}
