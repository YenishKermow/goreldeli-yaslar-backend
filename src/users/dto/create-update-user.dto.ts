import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateUpdateUserDto {
  @ApiProperty({ default: '+99362803369' })
  @IsString()
  @MaxLength(12)
  @MinLength(12)
  phone: string;

  @ApiProperty({ default: 'Arslan' })
  @IsString()
  firstname: string;

  @ApiProperty({ default: 'Abayew' })
  @IsString()
  lastname: string;

  @IsOptional()
  @IsString()
  password: string;

  @IsBoolean()
  isEdit: boolean;

  @IsBoolean()
  isLoading: boolean;
}
