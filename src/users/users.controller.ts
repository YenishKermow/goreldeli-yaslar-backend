import { PaginationQueryDto } from './../common/pagination/query.dto';
import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
  Res,
  Header,
  Req,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUpdateUserDto, CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Roles } from '../common/decorators/roles.decorators';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { Public } from '../common/decorators/public.decorator';

@ApiTags('Пользователи')
@ApiBearerAuth()
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('me')
  @Header('Access-Control-Expose-Headers', '*')
  async getMe(@Req() request: Request<any>) {
    return await this.usersService.findMe(request);
  }

  @Get('manager')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllManagers(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const managers = await this.usersService.findManagers(query);
    response.header('Content-range', managers.count.toString());
    response.send(managers.result);
  }

  @Get('cashier')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllCashiers(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const cashiers = await this.usersService.findCashiers(query);
    response.header('Content-range', cashiers.count.toString());
    response.send(cashiers.result);
  }

  @Get('director')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllDirectors(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const directors = await this.usersService.findDirectors(query);
    response.header('Content-range', directors.count.toString());
    response.send(directors.result);
  }

  @Get('agronomist')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllAgronomists(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const agronomist = await this.usersService.findAgronomists(query);
    response.header('Content-range', agronomist.count.toString());
    response.send(agronomist.result);
  }

  @Get('admin/dashbord')
  @Header('Access-Control-Expose-Headers', '*')
  async dashbordAdmin() {
    return this.usersService.dashbordDirectors();
  }

  @Get('manager/dashbord')
  @Header('Access-Control-Expose-Headers', '*')
  async dashbordManagers() {
    return this.usersService.dashbordManagers();
  }

  @Public()
  @Get('cashier/dashbord')
  @Header('Access-Control-Expose-Headers', '*')
  async dashbordCashiers() {
    return this.usersService.dashbordCashiers();
  }

  @Public()
  @Get('director/dashbord')
  @Header('Access-Control-Expose-Headers', '*')
  async dashbordDirectors() {
    return this.usersService.dashbordDirectors();
  }

  @Get('agronomist/dashbord')
  @Header('Access-Control-Expose-Headers', '*')
  async dashbordAgronomists() {
    return this.usersService.dashbordAgronomists();
  }

  @Roles('admin')
  @Post('manager')
  createManager(@Body() createUserDto: CreateUserDto) {
    return this.usersService.createManager(createUserDto);
  }

  @Roles('admin')
  @Post('cashier')
  createCashier(@Body() createUserDto: CreateUserDto) {
    return this.usersService.createCashier(createUserDto);
  }

  @Roles('admin')
  @Post('director')
  createDirector(@Body() createUserDto: CreateUserDto) {
    return this.usersService.createDirector(createUserDto);
  }

  @Roles('admin')
  @Post('agronomist')
  createAgronomist(@Body() createUserDto: CreateUserDto) {
    return this.usersService.createAgronomist(createUserDto);
  }

  @Post('update/me')
  updateAdmin(
    @Body() updateUserDto: any,
    @Req() request: Request<any>,
  ) {
    return this.usersService.updateMe(updateUserDto, request);
  }

  @Roles('admin')
  @Put('manager/:id')
  updateManager(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.updateManager(+id, updateUserDto);
  }

  @Roles('admin')
  @Put('cashier/:id')
  updateCashier(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.updateCashier(+id, updateUserDto);
  }

  @Roles('admin')
  @Put('director/:id')
  updateDirector(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.updateDirector(+id, updateUserDto);
  }

  @Roles('admin')
  @Put('agronomist/:id')
  updateAgronomist(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.updateAgronomist(+id, updateUserDto);
  }

  @Get('manager/:id')
  findManager(@Param('id') id: string) {
    return this.usersService.findOneManager(+id);
  }

  @Get('cashier/:id')
  findCashier(@Param('id') id: string) {
    return this.usersService.findOneCashier(+id);
  }

  @Get('director/:id')
  findDirector(@Param('id') id: string) {
    return this.usersService.findOneDirector(+id);
  }

  @Get('agronomist/:id')
  findAgronomist(@Param('id') id: string) {
    return this.usersService.findOneAgronomist(+id);
  }

  @Roles('admin')
  @Delete('manager/:id')
  deleteManager(@Param('id') id: string) {
    return this.usersService.removeManager(+id);
  }

  @Roles('admin')
  @Delete('cashier/:id')
  deleteCashier(@Param('id') id: string) {
    return this.usersService.removeCashier(+id);
  }

  @Roles('admin')
  @Delete('director/:id')
  deleteDirector(@Param('id') id: string) {
    return this.usersService.removeDirector(+id);
  }

  @Roles('admin')
  @Delete('agronomist/:id')
  deleteAgronomist(@Param('id') id: string) {
    return this.usersService.removeAgronomist(+id);
  }
}
