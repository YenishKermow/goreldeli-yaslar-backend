import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { Bcrypt } from '../common/utils/bcrypt';
import { PrismaService } from '../prisma.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService, Bcrypt, PrismaService],
  exports: [UsersService],
})
export class UsersModule {}
