import { PaginationQueryDto } from './../common/pagination/query.dto';
import { users } from '.prisma/client';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { Bcrypt } from '../common/utils/bcrypt';
import { PrismaService } from '../prisma.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(private readonly bcrypt: Bcrypt, private prisma: PrismaService) {}

  async findMe(request) {
    const user = request.user;

    const findMe = await this.prisma.users.findUnique({
      where: {
        uuid: user.uuid,
      },
      select: this.selectHelpers(),
    });

    return findMe;
  }

  async create(createUserDto) {
    const password = await this.bcrypt.bcryptCode(createUserDto.password);

    try {
      return await this.prisma.users.create({
        data: {
          ...createUserDto,
          password,
          agronomist:
            createUserDto.role === 'agronomist'
              ? this.createUserChild(createUserDto)
              : this.createUserChild(),
          manager:
            createUserDto.role === 'manager'
              ? this.createUserChild(createUserDto)
              : this.createUserChild(),
          cashier:
            createUserDto.role === 'cashier'
              ? this.createUserChild(createUserDto)
              : this.createUserChild(),

          director:
            createUserDto.role === 'director'
              ? this.createUserChild(createUserDto)
              : this.createUserChild(),
        },
        select: this.selectHelpers(),
      });
    } catch (error) {
      console.log(error);

      throw new BadRequestException(
        `${createUserDto.role} не добавился. Номер занят`,
      );
    }
  }

  async createManager(createUserDto: CreateUserDto) {
    const password = await this.bcrypt.bcryptCode(createUserDto.password);
    try {
      const user = await this.prisma.users.create({
        data: {
          ...createUserDto,
          role: 'manager',
          password,
          manager: this.createUserChild(createUserDto),
        },
        include: {
          manager: true,
        },
      });

      return user.manager;
    } catch (error) {
      console.log(error);
      throw new BadRequestException(`Менеджер не добавился. Номер занят`);
    }
  }

  async createCashier(createUserDto: CreateUserDto) {
    const password = await this.bcrypt.bcryptCode(createUserDto.password);

    try {
      const user = await this.prisma.users.create({
        data: {
          ...createUserDto,
          role: 'cashier',
          password,
          cashier: this.createUserChild(createUserDto),
        },
        include: {
          cashier: true,
        },
      });

      return user.cashier;
    } catch (error) {
      console.log(error);

      throw new BadRequestException(`Кассир не добавился. Номер занят`);
    }
  }

  async createDirector(createUserDto: CreateUserDto) {
    const password = await this.bcrypt.bcryptCode(createUserDto.password);

    try {
      const user = await this.prisma.users.create({
        data: {
          ...createUserDto,
          role: 'director',
          password,
          director: this.createUserChild(createUserDto),
        },
        include: {
          director: true,
        },
      });

      return user.director;
    } catch (error) {
      console.log(error);

      throw new BadRequestException(`Директор не добавился. Номер занят`);
    }
  }

  async createAgronomist(createUserDto: CreateUserDto) {
    const password = await this.bcrypt.bcryptCode(createUserDto.password);

    try {
      const user = await this.prisma.users.create({
        data: {
          ...createUserDto,
          role: 'agronomist',
          password,
          agronomist: this.createUserChild(createUserDto),
        },
        include: {
          agronomist: true,
        },
      });

      return user.agronomist;
    } catch (error) {
      console.log(error);

      throw new BadRequestException(`Аграном не добавился. Номер занят`);
    }
  }

  async findAll(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);

    const count = await this.prisma.users.count({
      where: {
        active: true,
      },
    });

    const users = await this.prisma.users.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
      },
      select: this.selectHelpers(),
    });

    return { result: users, count };
  }

  async findManagers(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    const { phone, firstname } = filter;

    const count: number = await this.prisma.manager.count({
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    const managers = await this.prisma.manager.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    return { result: managers, count };
  }

  async findCashiers(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    const { phone, firstname } = filter;

    const count: number = await this.prisma.cashier.count({
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    const cashiers = await this.prisma.cashier.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    return { result: cashiers, count };
  }

  async findDirectors(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    const { phone, firstname } = filter;

    const count: number = await this.prisma.director.count({
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    const directors = await this.prisma.director.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    return { result: directors, count };
  }

  async findAgronomists(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    const { phone, firstname } = filter;

    const count: number = await this.prisma.agronomist.count({
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    const agronomist = await this.prisma.agronomist.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        phone: {
          contains: phone,
        },
        firstname: {
          contains: firstname,
        },
      },
    });

    return { result: agronomist, count };
  }

  async findOneManager(id: number): Promise<Partial<users>> {
    const user = await this.prisma.manager.findMany({
      where: {
        id,
        active: true,
      },
    });

    if (!user.length) {
      throw new NotFoundException('Нет такого юзера!');
    }

    return user[0];
  }

  async findOneCashier(id: number): Promise<Partial<users>> {
    const user = await this.prisma.cashier.findMany({
      where: {
        id,
        active: true,
      },
    });

    if (!user.length) {
      throw new NotFoundException('Нет такого юзера!');
    }

    return user[0];
  }

  async findOneDirector(id: number): Promise<Partial<users>> {
    const user = await this.prisma.director.findMany({
      where: {
        id,
        active: true,
      },
    });

    if (!user.length) {
      throw new NotFoundException('Нет такого юзера!');
    }

    return user[0];
  }

  async findOneAgronomist(id: number): Promise<Partial<users>> {
    const user = await this.prisma.agronomist.findMany({
      where: {
        id,
        active: true,
      },
    });

    if (!user.length) {
      throw new NotFoundException('Нет такого юзера!');
    }

    return user[0];
  }

  async updateMe(updateUserDto: UpdateUserDto, request: any) {
    const user = await this.prisma.users.findUnique({
      where: { uuid: request.user.uuid },
      include: {
        admin: true,
        cashier: true,
        director: true,
        manager: true,
        agronomist: true,
      },
    });

    if (!user) {
      throw new NotFoundException('Нет такого юзера!');
    }

    try {
      if (user.role === 'admin') {
        const admin = await this.updateAdmin(user.admin.id, updateUserDto);
        return admin;
      } else if (user.role === 'manager') {
        const manager = await this.updateManager(
          user.manager.id,
          updateUserDto,
        );
        return manager;
      } else if (user.role === 'director') {
        const director = await this.updateDirector(
          user.director.id,
          updateUserDto,
        );
        return director;
      } else if (user.role === 'cashier') {
        const cashier = await this.updateCashier(
          user.cashier.id,
          updateUserDto,
        );
        return cashier;
      } else if (user.role === 'agronomist') {
        const agronomist = await this.updateAgronomist(
          user.agronomist.id,
          updateUserDto,
        );
        return agronomist;
      }
    } catch (error) {
      throw new BadRequestException('Номер занят');
    }
  }

  async updateAdmin(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<Partial<users>> {
    const admin = await this.prisma.admin.findUnique({
      where: { id },
      include: {
        users: true,
      },
    });

    if (!admin) {
      throw new NotFoundException('Нет такого юзера!');
    }

    try {
      const user = await this.prisma.users.update({
        where: {
          uuid: admin.uuid_user,
        },
        data: {
          firstname: updateUserDto.firstname,
          lastname: updateUserDto.lastname,
          phone: updateUserDto.phone,
          admin: {
            update: {
              firstname: updateUserDto.firstname,
              lastname: updateUserDto.lastname,
              phone: updateUserDto.phone,
            },
          },
        },
        select: {
          admin: true,
        },
      });

      return user.admin;
    } catch (error) {
      throw new BadRequestException('Номер занят');
    }
  }

  async updateManager(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<Partial<users>> {
    const manager = await this.prisma.manager.findUnique({
      where: { id },
      include: {
        users: true,
      },
    });

    if (!manager) {
      throw new NotFoundException('Нет такого юзера!');
    }

    const password = updateUserDto.password
      ? await this.bcrypt.bcryptCode(updateUserDto.password)
      : manager.users.password;

    try {
      const user = await this.prisma.users.update({
        where: {
          uuid: manager.uuid_user,
        },
        data: {
          firstname: updateUserDto.firstname,
          lastname: updateUserDto.lastname,
          phone: updateUserDto.phone,
          password,
          manager: {
            update: {
              firstname: updateUserDto.firstname,
              lastname: updateUserDto.lastname,
              phone: updateUserDto.phone,
            },
          },
        },
        select: {
          manager: true,
        },
      });

      return user.manager;
    } catch (error) {
      console.log(error);

      throw new BadRequestException('Номер занят');
    }
  }

  async updateCashier(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<Partial<users>> {
    const cashier = await this.prisma.cashier.findUnique({
      where: { id },
      include: {
        users: true,
      },
    });

    if (!cashier) {
      throw new NotFoundException('Нет такого юзера!');
    }

    const password = updateUserDto.password
      ? await this.bcrypt.bcryptCode(updateUserDto.password)
      : cashier.users.password;

    try {
      const user = await this.prisma.users.update({
        where: {
          uuid: cashier.uuid_user,
        },
        data: {
          firstname: updateUserDto.firstname,
          lastname: updateUserDto.lastname,
          phone: updateUserDto.phone,
          password,
          cashier: {
            update: {
              firstname: updateUserDto.firstname,
              lastname: updateUserDto.lastname,
              phone: updateUserDto.phone,
            },
          },
        },
        select: {
          cashier: true,
        },
      });

      return user.cashier;
    } catch (error) {
      throw new BadRequestException('Номер занят');
    }
  }

  async updateDirector(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<Partial<users>> {
    const director = await this.prisma.director.findUnique({
      where: { id },
      include: {
        users: true,
      },
    });

    if (!director) {
      throw new NotFoundException('Нет такого юзера!');
    }

    const password = updateUserDto.password
      ? await this.bcrypt.bcryptCode(updateUserDto.password)
      : director.users.password;

    try {
      const user = await this.prisma.users.update({
        where: {
          uuid: director.uuid_user,
        },
        data: {
          firstname: updateUserDto.firstname,
          lastname: updateUserDto.lastname,
          phone: updateUserDto.phone,
          password,
          director: {
            update: {
              firstname: updateUserDto.firstname,
              lastname: updateUserDto.lastname,
              phone: updateUserDto.phone,
            },
          },
        },
        select: {
          director: true,
        },
      });

      return user.director;
    } catch (error) {
      throw new BadRequestException('Номер занят');
    }
  }

  async updateAgronomist(
    id: number,
    updateUserDto: UpdateUserDto,
  ): Promise<Partial<users>> {
    const agronomist = await this.prisma.agronomist.findUnique({
      where: { id },
      include: {
        users: true,
      },
    });

    if (!agronomist) {
      throw new NotFoundException('Нет такого юзера!');
    }

    const password = updateUserDto.password
      ? await this.bcrypt.bcryptCode(updateUserDto.password)
      : agronomist.users.password;

    try {
      const user = await this.prisma.users.update({
        where: {
          uuid: agronomist.uuid_user,
        },
        data: {
          firstname: updateUserDto.firstname,
          lastname: updateUserDto.lastname,
          phone: updateUserDto.phone,
          password,
          agronomist: {
            update: {
              firstname: updateUserDto.firstname,
              lastname: updateUserDto.lastname,
              phone: updateUserDto.phone,
            },
          },
        },
        select: {
          agronomist: true,
        },
      });

      return user.agronomist;
    } catch (error) {
      throw new BadRequestException('Номер занят');
    }
  }

  async removeManager(id: number): Promise<Partial<users>> {
    const user = await this.prisma.manager.findUnique({
      where: {
        id,
      },
      include: {
        users: true,
      },
    });

    if (!user) {
      throw new NotFoundException('Нет такого юзера');
    }

    return await this.prisma.users.update({
      where: {
        uuid: user.uuid_user,
      },
      data: {
        phone: user.uuid_user + ' deleted',
        active: false,
        manager: {
          update: {
            phone: user.uuid_user + ' deleted',
            active: false,
          },
        },
      },
      select: this.selectHelpers(),
    });
  }

  async removeCashier(id: number): Promise<Partial<users>> {
    const user = await this.prisma.cashier.findUnique({
      where: {
        id,
      },
      include: {
        users: true,
      },
    });

    if (!user) {
      throw new NotFoundException('Нет такого юзера');
    }

    return await this.prisma.users.update({
      where: {
        uuid: user.uuid_user,
      },
      data: {
        phone: user.uuid_user + ' deleted',
        active: false,
        cashier: {
          update: {
            active: false,
            phone: user.uuid_user + ' deleted',
          },
        },
      },
      select: this.selectHelpers(),
    });
  }

  async removeDirector(id: number): Promise<Partial<users>> {
    const user = await this.prisma.director.findUnique({
      where: {
        id,
      },
      include: {
        users: true,
      },
    });

    if (!user) {
      throw new NotFoundException('Нет такого юзера');
    }

    return await this.prisma.users.update({
      where: {
        uuid: user.uuid_user,
      },
      data: {
        active: false,
        phone: user.uuid_user + ' deleted',
        director: {
          update: {
            active: false,
            phone: user.uuid_user + ' deleted',
          },
        },
      },
      select: this.selectHelpers(),
    });
  }

  async removeAgronomist(id: number): Promise<Partial<users>> {
    const user = await this.prisma.agronomist.findUnique({
      where: {
        id,
      },
      include: {
        users: true,
      },
    });

    if (!user) {
      throw new NotFoundException('Нет такого юзера');
    }

    return await this.prisma.users.update({
      where: {
        uuid: user.uuid_user,
      },
      data: {
        phone: user.uuid_user + ' deleted',
        active: false,
        agronomist: {
          update: {
            active: false,
            phone: user.uuid_user + ' deleted',
          },
        },
      },
      select: this.selectHelpers(),
    });
  }

  async dashbordManagers() {
    const customHouse = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'custom_house',
      },
    });

    const office = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'office',
      },
    });

    const dinningRoom = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'dinning_room',
      },
    });

    const productPlace = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'product_place',
      },
    });

    const warehouse = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'warehouse',
      },
    });

    const greenhouse = await this.prisma.greenhouse.count({
      where: {
        active: true,
      },
    });

    const renter = await this.prisma.renter.count({
      where: {
        active: true,
      },
    });

    return {
      customHouse,
      office,
      dinningRoom,
      productPlace,
      warehouse,
      greenhouse,
      renter,
    };
  }
  async dashbordCashiers() {
    const paid = await this.prisma.check.count({
      where: {
        active: true,
        status: true,
      },
    });

    const unPaid = await this.prisma.check.count({
      where: {
        active: true,
        status: false,
      },
    });

    return { checksPaid: paid, checksUnpaid: unPaid };
  }
  async dashbordDirectors() {
    const dashbordManager = await this.dashbordManagers();
    const dashbordCashier = await this.dashbordCashiers();
    const dashbordAgronomist = await this.dashbordAgronomists();

    return { ...dashbordManager, ...dashbordCashier, ...dashbordAgronomist };
  }
  async dashbordAgronomists() {
    return {
      reports: await this.prisma.report.count({ where: { active: true } }),
    };
  }

  createUserChild(createUserDto: CreateUserDto | void = null): any {
    if (createUserDto) {
      return {
        create: {
          firstname: createUserDto.firstname,
          lastname: createUserDto.lastname,
          phone: createUserDto.phone,
        },
      };
    } else {
      return {
        create: undefined,
      };
    }
  }

  selectHelpers() {
    return {
      id: true,
      uuid: true,
      phone: true,
      firstname: true,
      lastname: true,
      role: true,
      active: true,
    };
  }
}
