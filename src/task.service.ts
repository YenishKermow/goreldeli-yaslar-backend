import { PrismaService } from './prisma.service';
import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import * as moment from 'moment';

@Injectable()
export class TasksService {
  constructor(private prisma: PrismaService) {}

  @Cron('0 0-23/3 * * *')
  async createChecks() {
    const renters = await this.prisma.renter.findMany({
      where: {
        active: true,
        check: {
          some: {
            to: {
              gte: new Date().toISOString().split('T')[0],
            },
          },
        },
      },
      include: {
        place: true,
        check: true,
      },
    });

    let allRenters = await this.prisma.renter.findMany({
      where: {
        active: true,
        place: {
          startRent: {
            lte: new Date(),
          },
        },
      },
      include: {
        place: true,
        check: true,
      },
    });

    for (let i = 0; i < renters.length; i++) {
      for (let index = 0; index < allRenters.length; index++) {
        if (renters[i].id === allRenters[index].id) {
          allRenters.splice(index, 1);
        }
      }
    }

    // console.log(moment().add(1, 'month').toDate().toISOString().split('T')[0]);

    const startDate = moment().format().split('T')[0];
    const endDate = moment()
      .add(1, 'month')
      .toDate()
      .toISOString()
      .split('T')[0];

    for (let i = 0; i < allRenters.length; i++) {
      await this.prisma.check.create({
        data: {
          renter: {
            connect: {
              id: allRenters[i].id,
            },
          },
          placement: {
            connect: {
              id: allRenters[i].place.id,
            },
          },
          location: allRenters[i].place.location,
          totalArea: allRenters[i].place.totalArea,
          firstname: allRenters[i].firstname,
          lastname: allRenters[i].lastname,
          shop_name: allRenters[i].shopName,
          water: allRenters[i].place.water,
          amperage: allRenters[i].place.amperage,
          communal: allRenters[i].place.communal,
          number_contract: allRenters[i].number_contract,
          date: new Date(),
          in: startDate,
          to: endDate,
          payment_date: null,
          peni: '0',
          rent:
            (
              +allRenters[i].place.rent +
              +allRenters[i].place.communal +
              +allRenters[i].place.water +
              +allRenters[i].place.amperage
            ).toString(),
          visible: false,
        },
      });
    }
  }
}
