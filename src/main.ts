import { Bcrypt } from './common/utils/bcrypt';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismaService } from './prisma.service';
import * as morgan from 'morgan';
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  if (process.env.NODE_ENV === 'development') {
    app.use(morgan('tiny'));
  }

  app.setGlobalPrefix('api');
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Accept');
    next();
  });

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    }),
  );
  const options = new DocumentBuilder()
    .addBearerAuth()  
    .setTitle('Статистика Туркменистана')
    .setDescription('Static application')
    .setVersion('1.0')
    .build();

  const prisma = new PrismaService();
  const bcrypt = new Bcrypt();

  const admin = await prisma.users.findFirst({
    where: { phone: process.env.ADMIN_PHONE },
  });

  if (!admin) {
    const password = await bcrypt.bcryptCode(process.env.ADMIN_PASSWORD);

    await prisma.users.create({
      data: {
        phone: process.env.ADMIN_PHONE,
        firstname: process.env.ADMIN_FIRSTNAME,
        lastname: process.env.ADMIN_LASTNAME,
        role: 'admin',
        password,
        admin: {
          create: {
            firstname: process.env.ADMIN_FIRSTNAME,
            lastname: process.env.ADMIN_LASTNAME,
            phone: process.env.ADMIN_PHONE,
          },
        },
      },
    });
  }

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);
  await app.listen(3001);
}
bootstrap();
