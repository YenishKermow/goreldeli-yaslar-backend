import { Module } from '@nestjs/common';
import { PlaceService } from './place.service';
import { PlaceController } from './place.controller';
import { PrismaService } from '../prisma.service';
import { FormatDate } from '../utils/format-date';

@Module({
  controllers: [PlaceController],
  providers: [PlaceService, PrismaService, FormatDate],
  exports: [PlaceService],
})
export class PlaceModule {}
