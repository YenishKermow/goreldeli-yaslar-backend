import { UpdateRenterDto } from './dto/update-renter.dto';
import { CreateRenterDto } from './dto/create-renter.dto';
import { PaginationQueryDto } from './../common/pagination/query.dto';
import { greenhouse, placement } from '.prisma/client';
import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { CreateGreenhouseDto } from './dto/create-greenhouse.dto';
import { CreatePlaceDto } from './dto/create-place.dto';
import { UpdateGreenhouseDto } from './dto/update-greenhouse.dto';
import { UpdatePlaceDto } from './dto/update-place.dto';
import { renter } from '@prisma/client';
import { FormatDate } from '../utils/format-date';

import * as moment from 'moment';

@Injectable()
export class PlaceService {
  constructor(
    private prisma: PrismaService,
    private readonly formatDate: FormatDate,
  ) {}

  async createCustomHouse(
    createPlaceDto: CreatePlaceDto,
    request: any,
  ): Promise<placement> {
    const place = await this.prisma.placement.create({
      data: {
        location: createPlaceDto.location,
        totalArea: createPlaceDto.totalArea,
        rent: createPlaceDto.rent,
        startRent: createPlaceDto.startRent,
        status: createPlaceDto.status,
        communal: createPlaceDto.communal,
        water: createPlaceDto.water,
        amperage: createPlaceDto.amperage,
        category: 'custom_house',
        renter:
          createPlaceDto.status && createPlaceDto.renter
            ? { create: this.createRenter(createPlaceDto.renter) }
            : { create: undefined },
        manager: {
          connect: {
            uuid_user: request.user.uuid,
          },
        },
      },
      include: {
        renter: true,
      },
    });

    if (
      place.renter.length &&
      new Date() >= new Date(createPlaceDto.startRent)
    ) {
      await this.createCheck(place);
    }

    return place;
  }

  async createOffice(
    createPlaceDto: CreatePlaceDto,
    request: any,
  ): Promise<placement> {
    const place = await this.prisma.placement.create({
      data: {
        location: createPlaceDto.location,
        totalArea: createPlaceDto.totalArea,
        rent: createPlaceDto.rent,
        startRent: createPlaceDto.startRent,
        status: createPlaceDto.status,
        communal: createPlaceDto.communal,
        water: createPlaceDto.water,
        amperage: createPlaceDto.amperage,
        category: 'office',
        renter: createPlaceDto.status
          ? { create: this.createRenter(createPlaceDto.renter) }
          : { create: undefined },
        manager: {
          connect: {
            uuid_user: request.user.uuid,
          },
        },
      },
      include: {
        renter: true,
      },
    });

    if (
      place.renter.length &&
      new Date() >= new Date(createPlaceDto.startRent)
    ) {
      await this.createCheck(place);
    }

    return place;
  }

  async createProductPlace(
    createPlaceDto: CreatePlaceDto,
    request: any,
  ): Promise<placement> {
    const place = await this.prisma.placement.create({
      data: {
        location: createPlaceDto.location,
        totalArea: createPlaceDto.totalArea,
        rent: createPlaceDto.rent,
        startRent: createPlaceDto.startRent,
        status: createPlaceDto.status,
        communal: createPlaceDto.communal,
        water: createPlaceDto.water,
        amperage: createPlaceDto.amperage,
        category: 'product_place',
        renter: createPlaceDto.status
          ? { create: this.createRenter(createPlaceDto.renter) }
          : { create: undefined },
        manager: {
          connect: {
            uuid_user: request.user.uuid,
          },
        },
      },
      include: {
        renter: true,
      },
    });

    if (
      place.renter.length &&
      new Date() >= new Date(createPlaceDto.startRent)
    ) {
      await this.createCheck(place);
    }

    return place;
  }

  async createWarehouse(
    createPlaceDto: CreatePlaceDto,
    request: any,
  ): Promise<placement> {
    const place = await this.prisma.placement.create({
      data: {
        location: createPlaceDto.location,
        totalArea: createPlaceDto.totalArea,
        rent: createPlaceDto.rent,
        startRent: createPlaceDto.startRent,
        status: createPlaceDto.status,
        communal: createPlaceDto.communal,
        water: createPlaceDto.water,
        amperage: createPlaceDto.amperage,
        category: 'warehouse',
        renter: createPlaceDto.renter
          ? { create: this.createRenter(createPlaceDto.renter) }
          : { create: undefined },
        manager: {
          connect: {
            uuid_user: request.user.uuid,
          },
        },
      },
      include: {
        renter: true,
      },
    });

    if (
      place.renter.length &&
      new Date() >= new Date(createPlaceDto.startRent)
    ) {
      await this.createCheck(place);
    }

    return place;
  }

  async createDinningRoom(
    createPlaceDto: CreatePlaceDto,
    request: any,
  ): Promise<placement> {
    const place = await this.prisma.placement.create({
      data: {
        location: createPlaceDto.location,
        totalArea: createPlaceDto.totalArea,
        rent: createPlaceDto.rent,
        startRent: createPlaceDto.startRent,
        status: createPlaceDto.status,
        communal: createPlaceDto.communal,
        water: createPlaceDto.water,
        amperage: createPlaceDto.amperage,
        category: 'dinning_room',
        renter: createPlaceDto.status
          ? { create: this.createRenter(createPlaceDto.renter) }
          : { create: undefined },
        manager: {
          connect: {
            uuid_user: request.user.uuid,
          },
        },
      },
      include: {
        renter: true,
      },
    });

    if (
      place.renter.length &&
      new Date() >= new Date(createPlaceDto.startRent)
    ) {
      await this.createCheck(place);
    }

    return place;
  }

  async createPlace(createPlaceDto, request: any): Promise<placement> {
    const place = await this.prisma.placement.create({
      data: {
        location: createPlaceDto.location,
        totalArea: createPlaceDto.totalArea,
        rent: createPlaceDto.rent,
        startRent: createPlaceDto.startRent,
        status: createPlaceDto.status,
        category: createPlaceDto.category,
        communal: createPlaceDto.communal,
        water: createPlaceDto.water,
        amperage: createPlaceDto.amperage,
        renter: createPlaceDto.status
          ? { create: this.createRenter(createPlaceDto.renter) }
          : { create: undefined },
        manager: {
          connect: {
            uuid_user: request.user.uuid,
          },
        },
      },
      include: {
        renter: true,
      },
    });

    if (
      place.renter.length &&
      new Date() >= new Date(createPlaceDto.startRent)
    ) {
      await this.createCheck(place);
    }

    return place;
  }

  createGreenhouse(
    createGreenhouseDto: CreateGreenhouseDto,
    request: any,
  ): Promise<greenhouse> {
    return this.prisma.greenhouse.create({
      data: {
        location: createGreenhouseDto.location,
        totalArea: createGreenhouseDto.totalArea,
        name: createGreenhouseDto.name,
        product: createGreenhouseDto.product,
        manager: {
          connect: {
            uuid_user: request.user.uuid,
          },
        },
      },
    });
  }

  async findAll(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);

    const count = await this.prisma.placement.count({
      where: {
        active: true,
      },
    });

    const places = await this.prisma.placement.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });

    return { result: places, count };
  }

  async findAllGreenhouse(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    let { location, totalArea, name } = filter;

    const count = await this.prisma.greenhouse.count({
      where: {
        active: true,
        location: {
          contains: location,
        },
        totalArea: {
          contains: totalArea,
        },
        name: {
          contains: name,
        },
      },
    });

    const greenhouses = await this.prisma.greenhouse.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        location: {
          contains: location,
        },
        totalArea: {
          contains: totalArea,
        },
        name: {
          contains: name,
        },
      },
    });

    return { result: greenhouses, count };
  }

  async findAllCustom(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    let { renter, startRent, rent, phone } = filter;

    rent = rent ? rent.toString() : undefined;

    const count = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'custom_house',
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
    });

    const customHouses = await this.prisma.placement.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        category: 'custom_house',
        active: true,
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });

    return { result: customHouses, count };
  }

  async findAllOffice(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    let { renter, startRent, rent, phone } = filter;

    rent = rent ? rent.toString() : undefined;

    const count = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'office',
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
    });

    const offieces = await this.prisma.placement.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        category: 'office',
        active: true,
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });

    return { result: offieces, count };
  }

  async findAllProductPlace(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    let { renter, startRent, rent, phone } = filter;

    rent = rent ? rent.toString() : undefined;

    const count = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'product_place',
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
    });

    const productPlaces = await this.prisma.placement.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        category: 'product_place',
        active: true,
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });

    return { result: productPlaces, count };
  }

  async findAllWarehouse(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    let { renter, startRent, rent, phone } = filter;

    rent = rent ? rent.toString() : undefined;

    const count = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'warehouse',
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
    });

    const warehouses = await this.prisma.placement.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        category: 'warehouse',
        active: true,
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });

    return { result: warehouses, count };
  }

  async findAllDinningRoom(query: PaginationQueryDto) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    let { renter, startRent, rent, phone } = filter;

    rent = rent ? rent.toString() : undefined;

    const count = await this.prisma.placement.count({
      where: {
        active: true,
        category: 'dinning_room',
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
    });

    const dinningRooms = await this.prisma.placement.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        category: 'dinning_room',
        active: true,
        renter: (() => {
          return renter || phone
            ? {
                some: {
                  phone: {
                    contains: phone,
                  },
                  firstname: {
                    contains: renter,
                  },
                },
              }
            : undefined;
        })(),
        startRent: this.formatDate.checkDate(startRent),
        rent: {
          contains: rent,
        },
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });

    return { result: dinningRooms, count };
  }

  async findOnePlacement(id: number): Promise<placement> {
    const place = await this.prisma.placement.findMany({
      where: {
        id,
        active: true,
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
        checks: true,
      },
    });

    if (!place.length) {
      throw new NotFoundException('Нет такого участка');
    }

    return place[0];
  }

  async findOneGreenhouse(id: number): Promise<greenhouse> {
    const greenhouse = await this.prisma.greenhouse.findMany({
      where: {
        id,
        active: true,
      },
      include: {
        reports: true,
      },
    });

    if (!greenhouse.length) {
      throw new NotFoundException('Нет такого огорода');
    }

    return greenhouse[0];
  }

  async updatePlacement(
    id: number,
    updatePlaceDto: UpdatePlaceDto,
  ): Promise<placement> {
    const startDate = new Date().toISOString().split('T')[0];
    const getFormatDate = startDate.split('-');
    const endDate = new Date(
      `${getFormatDate[0]}-${
        new Date().getMonth() === 11 ? '01' : '0' + (new Date().getMonth() + 2)
      }-${getFormatDate[2]}`,
    )
      .toISOString()
      .split('T')[0];

    const place = await this.prisma.placement.update({
      where: { id },
      data: {
        location: updatePlaceDto.location,
        totalArea: updatePlaceDto.totalArea,
        communal: updatePlaceDto.communal,
        amperage: updatePlaceDto.amperage,
        water: updatePlaceDto.water,
        rent: updatePlaceDto.rent,
        startRent: updatePlaceDto.startRent,
        status: updatePlaceDto.status,
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });

    if (updatePlaceDto.renter.length <= place.renter.length) {
      const transformRenters = [];

      updatePlaceDto.renter.map((renter) => {
        transformRenters.push({ ...renter });
      });

      for (let i = 0; i < place.renter.length; i++) {
        const findRenter = transformRenters.find(
          (renter) => renter.id === place.renter[i].id,
        );

        if (findRenter) {
          await this.updateRenter(findRenter.id, place.id, findRenter);
        } else {
          await this.prisma.renter.updateMany({
            where: {
              id: place.renter[i].id,
              id_place: place.id,
            },
            data: {
              active: false,
            },
          });
        }
      }
    } else {
      const transformRenters = [];
      for (let i = 0; i < updatePlaceDto.renter.length; i++) {
        transformRenters.push({ ...updatePlaceDto.renter[i] });
      }

      const findRenter: CreateRenterDto[] = transformRenters.filter(
        (renter) => {
          if (renter.id_place !== place.id) {
            return renter;
          }
        },
      );

      for (let index = 0; index < findRenter.length; index++) {
        const sumRent =
          +place.rent + +place.water + +place.communal + +place.amperage;

        await this.prisma.renter.create({
          data: {
            firstname: findRenter[index].firstname,
            lastname: findRenter[index].lastname,
            shopName: findRenter[index].shopName,
            number_contract: findRenter[index].number_contract,
            patronymic: findRenter[index].patronymic,
            phone: findRenter[index].phone,
            place: {
              connect: {
                id: place.id,
              },
            },
            check: {
              create: {
                location: place.location,
                totalArea: place.totalArea,
                firstname: findRenter[index].firstname,
                lastname: findRenter[index].lastname,
                shop_name: findRenter[index].shopName,
                number_contract: findRenter[index].number_contract,
                amperage: place.amperage,
                communal: place.communal,
                water: place.water,
                date: new Date(),
                payment_date: null,
                peni: '0',
                rent: sumRent.toString(),
                in: startDate,
                to: endDate,
                placement: {
                  connect: {
                    id: place.id,
                  },
                },
              },
            },
          },
        });
      }
    }

    return await this.prisma.placement.findUnique({
      where: {
        id,
      },
      include: {
        renter: {
          where: {
            active: true,
          },
        },
      },
    });
  }

  updateGreenhouse(
    id: number,
    updateGreenhouseDto: UpdateGreenhouseDto,
  ): Promise<greenhouse> {
    return this.prisma.greenhouse.update({
      where: { id },
      data: {
        product: updateGreenhouseDto.product,
        name: updateGreenhouseDto.name,
        totalArea: updateGreenhouseDto.totalArea,
        location: updateGreenhouseDto.location,
      },
    });
  }

  removePlacement(id: number) {
    return this.prisma.placement.update({
      where: { id },
      data: {
        active: false,
        renter: {
          updateMany: {
            where: {
              id_place: id,
            },
            data: {
              active: false,
            },
          },
        },
      },
    });
  }

  async removeGreenhouse(id: number) {
    const greenhouse = await this.prisma.greenhouse.update({
      where: { id },
      data: {
        active: false,
      },
    });

    return greenhouse;
  }

  createRenter(renters: CreateRenterDto[]): renter[] {
    const renterArray = [];
    for (let i = 0; i < renters.length; i++) {
      renterArray.push(renters[i]);
    }

    return renterArray;
  }

  async createCheck(place) {

    const startDate = moment().format().split('T')[0];
    const endDate = moment().add(1, 'month').toDate().toISOString().split('T')[0]

    for (let i = 0; i < place.renter.length; i++) {
      await this.prisma.check.create({
        data: {
          renter: {
            connect: {
              id: place.renter[i].id,
            },
          },
          placement: {
            connect: {
              id: place.id,
            },
          },
          location: place.location,
          totalArea: place.totalArea,
          communal: place.communal,
          water: place.water,
          amperage: place.amperage,
          firstname: place.renter[i].firstname,
          lastname: place.renter[i].lastname,
          shop_name: place.renter[i].shopName,
          number_contract:  place.renter[i].number_contract,
          in: startDate,
          to: endDate,
          date: new Date(),
          payment_date: null,
          peni: '0',
          rent:
            (
              +place.rent +
              +place.communal +
              +place.water +
              +place.amperage
            ).toString(),
        },
      });
    }
  }

  async findCheks(query) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);

    const count = await this.prisma.check.count({
      where: {
        active: true,
      },
    });

    const checks = await this.prisma.check.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
      },
    });

    return { result: checks, count };
  }

  async updateRenter(
    id: number,
    idPlace: number,
    updateRenterDto: UpdateRenterDto,
  ) {
    const renter = await this.prisma.renter.updateMany({
      where: {
        id,
        id_place: idPlace,
      },
      data: {
        firstname: updateRenterDto.firstname,
        lastname: updateRenterDto.lastname,
        shopName: updateRenterDto.shopName,
        number_contract: updateRenterDto.number_contract,
        phone: updateRenterDto.phone,
        patronymic: updateRenterDto.patronymic,
      },
    });

    return renter;
  }
}
