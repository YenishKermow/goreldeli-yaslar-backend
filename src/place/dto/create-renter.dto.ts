import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class CreateRenterDto {
  @ApiProperty({ default: 'Iwan' })
  @IsString()
  firstname: string;

  @ApiProperty({ default: 'Iwanow' })
  @IsString()
  lastname: string;

  @ApiProperty({ default: 'Iwanowich' })
  @IsOptional()
  patronymic: string;

  @ApiProperty({ default: 'Archalyk' })
  @IsString()
  shopName: string;

  @ApiProperty({ default: '000001' })
  @IsString()
  number_contract: string;

  @ApiProperty({ default: '+99362222222' })
  @IsString()
  phone: string;
}
