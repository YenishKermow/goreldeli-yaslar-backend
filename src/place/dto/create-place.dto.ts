import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBoolean, IsDate, IsNumber, IsString, ValidateNested } from 'class-validator';
import { CreateRenterDto } from './create-renter.dto';

export class CreatePlaceDto {
  @ApiProperty({ default: 'Sector A' })
  @IsString()
  location: string;

  @ApiProperty({ default: '2000 metr/kw' })
  @IsString()
  totalArea: string;

  @ApiProperty({ default: '2000 TMT' })
  @IsString()
  rent: string;
  
  @ApiProperty()
 @IsString()
  communal: string;

  @ApiProperty()
 @IsString()
  amperage: string;

  @ApiProperty()
 @IsString()
  water: string;

  @ApiProperty({ default: new Date() })
  @IsDate()
  @Type(() => Date)
  startRent: Date;

  @ApiProperty({ default: true })
  @IsBoolean()
  status: boolean;

  @ApiProperty({ type: () => [CreateRenterDto] })
  @ValidateNested({ each: true })
  @Type(() => CreateRenterDto)
  renter: CreateRenterDto[];
}
