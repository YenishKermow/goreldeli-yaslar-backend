import { PartialType } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';
import { CreateGreenhouseDto } from './create-greenhouse.dto';

export class UpdateGreenhouseDto extends PartialType(CreateGreenhouseDto) {
  @IsBoolean()
  @IsOptional()
  active?: boolean;

  @IsOptional()
  @IsNumber()
  id?: number;

  
  @IsOptional()
  @IsString()
  id_manager?: string;

  @IsOptional()
  reports?: [];
}
