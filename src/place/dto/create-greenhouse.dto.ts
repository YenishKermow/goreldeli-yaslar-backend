import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateGreenhouseDto {
  @ApiProperty({ default: 'Sector A' })
  @IsString()
  location: string;

  @ApiProperty({ default: '2000 metr/kw' })
  @IsString()
  totalArea: string;

  @ApiProperty({ default: 'Alma' })
  @IsString()
  product: string;

  @ApiProperty({ default: 'name parnik' })
  @IsString()
  name: string;
}
