import { UpdateRenterDto } from './update-renter.dto';
import { OmitType, PartialType } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';
import { CreatePlaceDto } from './create-place.dto';

export class UpdatePlaceDto extends PartialType(
  OmitType(CreatePlaceDto, ['renter'] as const),
) {
  @IsOptional()
  @IsNumber()
  id?: number;

  @IsOptional()
  @IsBoolean()
  active?: boolean;

  @IsOptional()
  @IsString()
  uuid?: string;

  @IsOptional()
  @IsString()
  uuid_user?: string;

  @IsOptional()
  @IsString()
  category?: string;

  @IsOptional()
  @IsString()
  uuid_manager?: string;

  @IsOptional()
  renter?: UpdateRenterDto[];

  @IsOptional()
  checks?: [];
}
