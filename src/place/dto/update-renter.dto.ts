import { CreateRenterDto } from './create-renter.dto';
import { PartialType } from '@nestjs/swagger';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateRenterDto extends PartialType(CreateRenterDto) {
  @IsBoolean()
  @IsOptional()
  active?: boolean;

  @IsOptional()
  @IsNumber()
  id?: number;

  @IsOptional()
  @IsString()
  uuid_place?: string;
}
