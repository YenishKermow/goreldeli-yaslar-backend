import { PaginationQueryDto } from './../common/pagination/query.dto';
import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Req,
  Query,
  Res,
  Header,
} from '@nestjs/common';
import { PlaceService } from './place.service';
import { CreatePlaceDto } from './dto/create-place.dto';
import { UpdatePlaceDto } from './dto/update-place.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { CreateGreenhouseDto } from './dto/create-greenhouse.dto';
import { Request, Response } from 'express';
import { Roles } from '../common/decorators/roles.decorators';
import { UpdateGreenhouseDto } from './dto/update-greenhouse.dto';

@ApiTags('Помещении')
@ApiBearerAuth()
@Controller('places')
export class PlaceController {
  constructor(private readonly placeService: PlaceService) {}

  @Roles('manager')
  @Post('custom-house')
  createCustomHouse(
    @Body() createPlaceDto: CreatePlaceDto,
    @Req() request: Request<any>,
  ) {
    return this.placeService.createCustomHouse(createPlaceDto, request);
  }

  @Roles('manager')
  @Post('office')
  createOffice(
    @Body() createPlaceDto: CreatePlaceDto,
    @Req() request: Request<any>,
  ) {
    return this.placeService.createOffice(createPlaceDto, request);
  }

  @Roles('manager')
  @Post('product-place')
  createProductPlace(
    @Body() createPlaceDto: CreatePlaceDto,
    @Req() request: Request<any>,
  ) {
    return this.placeService.createProductPlace(createPlaceDto, request);
  }

  @Roles('manager')
  @Post('warehouse')
  createWarehouse(
    @Body() createPlaceDto: CreatePlaceDto,
    @Req() request: Request<any>,
  ) {
    return this.placeService.createWarehouse(createPlaceDto, request);
  }

  @Roles('manager')
  @Post('dinning-room')
  createDinningRoom(
    @Body() createPlaceDto: CreatePlaceDto,
    @Req() request: Request<any>,
  ) {
    return this.placeService.createDinningRoom(createPlaceDto, request);
  }

  @Get()
  @Header('Access-Control-Expose-Headers', '*')
  async findAll(@Query() query: PaginationQueryDto, @Res() response: Response) {
    const places = await this.placeService.findAll(query);
    response.header('Content-range', places.count.toString());
    response.send(places.result);
  }

  @Get('checks')
  @Header('Access-Control-Expose-Headers', '*')
  async findCheck(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const checks = await this.placeService.findCheks(query);
    response.header('Content-range', checks.count.toString());
    response.send(checks.result);
  }

  @Roles('manager')
  @Post('greenhouse')
  createGreenhouse(
    @Body() createGreenhouseDto: CreateGreenhouseDto,
    @Req() request: Request<any>,
  ) {
    return this.placeService.createGreenhouse(createGreenhouseDto, request);
  }

  @Get('greenhouse')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllGreenhouse(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const greenhouse = await this.placeService.findAllGreenhouse(query);
    response.header('Content-range', greenhouse.count.toString());
    response.send(greenhouse.result);
  }

  @Get('custom-house')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllCustom(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const customHouses = await this.placeService.findAllCustom(query);
    response.header('Content-range', customHouses.count.toString());
    response.send(customHouses.result);
  }

  @Get('office')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllOffice(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const offices = await this.placeService.findAllOffice(query);
    response.header('Content-range', offices.count.toString());
    response.send(offices.result);
  }

  @Get('product-place')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllProductPlace(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const productPlaces = await this.placeService.findAllProductPlace(query);
    response.header('Content-range', productPlaces.count.toString());
    response.send(productPlaces.result);
  }

  @Get('warehouse')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllWarehouse(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const warehouses = await this.placeService.findAllWarehouse(query);
    response.header('Content-range', warehouses.count.toString());
    response.send(warehouses.result);
  }

  @Get('dinning-room')
  @Header('Access-Control-Expose-Headers', '*')
  async findAllDinningRoom(
    @Query() query: PaginationQueryDto,
    @Res() response: Response,
  ) {
    const dinningRooms = await this.placeService.findAllDinningRoom(query);
    response.header('Content-range', dinningRooms.count.toString());
    response.send(dinningRooms.result);
  }

  @Get('custom-house/:id')
  findCustomHouse(@Param('id') id: string) {
    return this.placeService.findOnePlacement(+id);
  }

  @Get('office/:id')
  findOffice(@Param('id') id: string) {
    return this.placeService.findOnePlacement(+id);
  }

  @Get('product-place/:id')
  findProductPlace(@Param('id') id: string) {
    return this.placeService.findOnePlacement(+id);
  }

  @Get('warehouse/:id')
  findWarehouse(@Param('id') id: string) {
    return this.placeService.findOnePlacement(+id);
  }

  @Get('dinning-room/:id')
  findDinningRoom(@Param('id') id: string) {
    return this.placeService.findOnePlacement(+id);
  }

  @Get('greenhouse/:id')
  findOneGreenhouse(@Param('id') id: string) {
    return this.placeService.findOneGreenhouse(+id);
  }

  @Roles('manager')
  @Put('custom-house/:id')
  updateCustomHouse(
    @Param('id') id: string,
    @Body() updatePlaceDto: UpdatePlaceDto,
  ) {
    return this.placeService.updatePlacement(+id, updatePlaceDto);
  }

  @Roles('manager')
  @Put('office/:id')
  updateOffice(
    @Param('id') id: string,
    @Body() updatePlaceDto: UpdatePlaceDto,
  ) {
    return this.placeService.updatePlacement(+id, updatePlaceDto);
  }

  @Roles('manager')
  @Put('product-place/:id')
  updateProductPlace(
    @Param('id') id: string,
    @Body() updatePlaceDto: UpdatePlaceDto,
  ) {
    return this.placeService.updatePlacement(+id, updatePlaceDto);
  }

  @Roles('manager')
  @Put('warehouse/:id')
  updateWarehouse(
    @Param('id') id: string,
    @Body() updatePlaceDto: UpdatePlaceDto,
  ) {
    return this.placeService.updatePlacement(+id, updatePlaceDto);
  }

  @Roles('manager')
  @Put('dinning-room/:id')
  updateDinningRoom(
    @Param('id') id: string,
    @Body() updatePlaceDto: UpdatePlaceDto,
  ) {
    return this.placeService.updatePlacement(+id, updatePlaceDto);
  }

  @Roles('manager')
  @Put('greenhouse/:id')
  updategreeenhouse(
    @Param('id') id: string,
    @Body() updateGreenhouseDto: UpdateGreenhouseDto,
  ) {
    return this.placeService.updateGreenhouse(+id, updateGreenhouseDto);
  }

  @Roles('manager')
  @Delete('custom-house/:id')
  removeCustomHouse(@Param('id') id: string) {
    return this.placeService.removePlacement(+id);
  }

  @Roles('manager')
  @Delete('office/:id')
  removeOffice(@Param('id') id: string) {
    return this.placeService.removePlacement(+id);
  }

  @Roles('manager')
  @Delete('product-place/:id')
  removeProductPlace(@Param('id') id: string) {
    return this.placeService.removePlacement(+id);
  }

  @Roles('manager')
  @Delete('warehouse/:id')
  removeWarehouse(@Param('id') id: string) {
    return this.placeService.removePlacement(+id);
  }

  @Roles('manager')
  @Delete('dinning-room/:id')
  removeDinningRoom(@Param('id') id: string) {
    return this.placeService.removePlacement(+id);
  }

  @Roles('manager')
  @Delete('greenhouse/:id')
  removeGreenhouse(@Param('id') id: string) {
    return this.placeService.removeGreenhouse(+id);
  }
}
