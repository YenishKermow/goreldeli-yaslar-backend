import { Controller, Get, Post, Body, Param, Delete, Req, Res, Query, Header, Put } from '@nestjs/common';
import { ReportService } from './report.service';
import { CreateReportDto } from './dto/create-report.dto';
import { Roles } from '../common/decorators/roles.decorators';
import { roles } from '@prisma/client';
import { Request, Response } from 'express';
import { PaginationQueryDto } from '../common/pagination/query.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Отчеты по парнику')
@Controller('report')
export class ReportController {
  constructor(private readonly reportService: ReportService) {}

  @Roles(roles.agronomist)
  @Post()
  create(@Body() createReportDto: CreateReportDto,  @Req() request: Request<any>, @Query() query: PaginationQueryDto,) {
    return this.reportService.create(query, createReportDto, request.user);
  }

  @Header('Access-Control-Expose-Headers', '*')
  @Get()
  async findAll(@Query() query: PaginationQueryDto, @Res() response: Response) {
    const reports =  await this.reportService.findAll(query);
    response.header('Content-range', reports.count.toString());
    response.send(reports.result);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reportService.findOne(+id);
  }

  @Roles(roles.agronomist)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateReportDto) {
    return this.reportService.update(+id, updateReportDto);
  }

  @Roles(roles.agronomist)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reportService.remove(+id);
  }
}
