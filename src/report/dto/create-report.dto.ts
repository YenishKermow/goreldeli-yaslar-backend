import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsNumber, IsString, Min } from 'class-validator';

export class CreateReportDto {
  @ApiProperty()
  @IsNumber()
  greenhouse_id: number;

  @ApiProperty()
  @IsString()
  product: string;
 
  @ApiProperty()
  @IsNumber()
  @Min(0)
  weight: number;

  @ApiProperty()
  @IsNumber()
  @Min(0)
  price: number;
}
