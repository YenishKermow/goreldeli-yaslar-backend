import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsDateString } from 'class-validator';
import { CreateReportDto } from './create-report.dto';

export class UpdateReportDto extends PartialType(CreateReportDto) {
  @ApiProperty()
  @IsDateString()
  date: Date;
}
