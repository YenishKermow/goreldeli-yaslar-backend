import { Module } from '@nestjs/common';
import { ReportService } from './report.service';
import { ReportController } from './report.controller';
import { PrismaService } from '../prisma.service';
import { FormatDate } from '../utils/format-date';

@Module({
  controllers: [ReportController],
  providers: [ReportService, PrismaService, FormatDate],
  exports: [ReportService]
})
export class ReportModule {}
