import { Injectable, NotFoundException } from '@nestjs/common';
import { FormatDate } from '../utils/format-date';
import { PaginationQueryDto } from '../common/pagination/query.dto';
import { PrismaService } from '../prisma.service';
import { CreateReportDto } from './dto/create-report.dto';

@Injectable()
export class ReportService {
  constructor(
    private prisma: PrismaService,
    private readonly formatDate: FormatDate,
  ) {}

  async create(
    query: PaginationQueryDto,
    createReportDto: CreateReportDto,
    user,
  ) {
    const greenhouse = await this.prisma.greenhouse.findUnique({
      where: {
        id: createReportDto.greenhouse_id,
      },
    });

    return this.prisma.report.create({
      data: {
        location: greenhouse.location,
        name: greenhouse.name,
        product: greenhouse.product,
        weight: createReportDto.weight,
        price: createReportDto.price,
        totalArea: greenhouse.totalArea,
        date: new Date(),
        sum: parseFloat(
          (createReportDto.price * createReportDto.weight).toFixed(2),
        ),
        agronomist: {
          connect: {
            uuid_user: user.uuid,
          },
        },
        greenhouse: {
          connect: {
            id: greenhouse.id,
          },
        },
      },
    });
  }

  async findAll(query) {
    const range = JSON.parse(query.range);
    const sort: string[] = JSON.parse(query.sort);
    const filter = JSON.parse(query.filter);

    const { product, location, id, price } = filter;

    const count = await this.prisma.report.count({
      where: {
        active: true,
        product: {
          contains: product,
        },
        location: {
          contains: location,
        },
        greenhouse: {
          id,
        },
        price: {
          gte: price,
        },
      },
    });

    const reports = await this.prisma.report.findMany({
      skip: JSON.parse(range[0]),
      take: JSON.parse(range[1]) - JSON.parse(range[0]) + 1,
      orderBy:
        sort.length === 2
          ? { [sort[0]]: sort[1].toLowerCase() }
          : { id: 'asc' },
      where: {
        active: true,
        location: {
          contains: location,
        },
        greenhouse: {
          id,
        },
        price: {
          gte: price,
        },
        product: {
          contains: product,
        },
      },
    });

    return { result: reports, count };
  }

  async findOne(id: number) {
    const report = await this.prisma.report.findUnique({
      where: {
        id,
      },
      include: {
        agronomist: {
          select: {
            firstname: true,
            lastname: true,
            phone: true,
          },
        },
      },
    });

    if (!report) {
      throw new NotFoundException('Нет такого отчета');
    }

    return report;
  }

  update(id: number, updateReportDto) {
    const { greenhouseId, agronomistId, agronomist, ...result } =
      updateReportDto;

    return this.prisma.report.update({
      where: {
        id,
      },
      data: {
        sum: parseFloat(
          (updateReportDto.price * updateReportDto.weight).toFixed(2),
        ),
        price: updateReportDto.price,
        weight: updateReportDto.weight,
        product: updateReportDto.product,
        date: this.formatDate.checkDate(updateReportDto.date)
      },
    });
  }

  remove(id: number) {
    return this.prisma.report.update({
      where: { id },
      data: { active: false },
    });
  }

  createForInitData(createReportDto) {
    const { uuidUser, idGreenhouse, weight, ...result } = createReportDto;
    return this.prisma.report.create({
      data: {
        ...result,
        weight: parseFloat(weight),
        sum: parseFloat(
          (createReportDto.price * createReportDto.weight).toFixed(2),
        ),
        agronomist: {
          connect: {
            uuid_user: uuidUser,
          },
        },
        greenhouse: {
          connect: {
            id: idGreenhouse,
          },
        },
      },
    });
  }
}
