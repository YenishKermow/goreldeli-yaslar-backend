import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { ApiKeyGuard } from './guards/api-key.guard';
import { JwtModule } from '@nestjs/jwt';
import { PrismaService } from '../prisma.service';

@Module({
  imports: [
    JwtModule.registerAsync({
      useFactory: async () => ({
        secret: process.env.SECRET_TOKEN,
      }),
    }),
  ],
  providers: [{ provide: APP_GUARD, useClass: ApiKeyGuard }, PrismaService],
})
export class CommonModule {}
