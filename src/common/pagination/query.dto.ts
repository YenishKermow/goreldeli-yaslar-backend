import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class PaginationQueryDto {
  @ApiProperty({ default: {} })
  @IsOptional()
  filter: string;

  @ApiProperty({ default: '[0,24]' })
  @IsOptional()
  range: string;

  @ApiProperty({ default: `["id","DESC"]` })
  @IsOptional()
  sort: string;
}
