import { ROLES_KEY } from './../decorators/roles.decorators';
import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { JwtService } from '@nestjs/jwt';
import { roles } from '@prisma/client';
import { PrismaService } from '../../prisma.service';
import { IS_PUBLIC_KEY } from '../decorators/public.decorator';

@Injectable()
export class ApiKeyGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly jwtService: JwtService,
    private prisme: PrismaService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<roles[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    const isPublic = this.reflector.get(IS_PUBLIC_KEY, context.getHandler());

    if (isPublic) return true;

    try {
      const request = context.switchToHttp().getRequest<Request>();
      const token: string = request.header('Authorization').split(' ')[1];
      this.jwtService.verify(token);
      const verifyToken = this.jwtService.verify(token);
      request.user = verifyToken;
      const { user } = context.switchToHttp().getRequest();
      const findUser = await this.prisme.users.findUnique({
        where: { uuid: user.uuid },
      });

      if (findUser && !requiredRoles) {
        return true;
      }

      if (!findUser) {
        return false;
      }
      return requiredRoles.some((role) => user.role?.includes(role));
    } catch (error) {
      throw new UnauthorizedException('Invalid Token');
    }
  }
}
