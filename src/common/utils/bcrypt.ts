import * as bcrypt from 'bcrypt';

export class Bcrypt {
  async bcryptCode(password: string) {
    const hash = await bcrypt.hash(password, +process.env.SECRET_KEY);
    return hash;
  }

  async decryptCode(password: string, hash: string) {
    const isMatch = await bcrypt.compare(password, hash);
    return isMatch;
  }
}
